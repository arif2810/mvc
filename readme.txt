1. npm install -g express-generator
2. express --view ejs --git nama_project
3. Masuk ke directory: cd nama_project
4. npm install
5. npm start

Install Database
1. npm install sequelize-cli -g
2. npm init -y
3. npm install sequelize
4. sequelize init --> ada 4 folder created
5. pengaturan config disesuaikan dengan database
6. sequelize db:create --> error karena belum install adapter
7. npm install pg --> install adaptor
8. sequelize db:create --> membuat database pada postgress
9. sequelize model:generate --name nama_tabel --attributes attribute1:tipe_data_attribute1,attribute2:tipe_data_attribute2,attribute3:tipe_data_attribute3 --> membuat model, akan terbuat file model pada folder models
10. edit file model sesuaikan lagi dengan tipe data yg dibutuhkan.
11. sequelize db:migrate --> memindahkan skema tabel ke database





